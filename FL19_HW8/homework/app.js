// #1
function extractCurrencyValue(param) {
    let result = BigInt;
    if (param.length < 16){
  return parseInt(param);
    } else { 
        result = BigInt(parseInt(param));
        return result;
           }
}

console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) { 
    
 for (let i in obj) {
  if (obj[i] === null || obj[i] === undefined || obj[i] === false) {
  delete obj[i];
  }
 } return obj;

}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    param = Symbol("'" + param + "'");
    return param;
    
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
const date1 = new Date(startDate);
const date2 = new Date(endDate);

  
const daydiff = parseFloat(Math.round((date2-date1)/(24*3600*1000))); 
const weeks = Math.round(daydiff / 7);
const month = Math.round(weeks / 4);


  return `The difference between dates is: ${daydiff} day(-s), ${weeks} week(-s), ${month} month(-s)` 
}

console.log(countBetweenTwoDays('03/22/22', '05/25/22')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
let res = [...new Set(arr)];
    
return res;
}

console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]

//5.2
function filterArraySecond(arr2) {
let res = [];
    for (let i=0; i<arr2.length; i++){
        if (res.indexOf(arr2[i]) === -1 && arr2[i] !== ''){
            res.push(arr2[i]);
        
        }
    }
    return res;
}
console.log(filterArraySecond([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9]));
