//1
function getWeekDay(){
return new Date().toLocaleString('en-us', { weekday: 'long' })
}
console.log(getWeekDay(Date.now())); // "Thursday" (if today is the 22nd October)
console.log(getWeekDay(new Date(2020, 9, 22))); // "Thursday"


//2
function getAmountDaysToNewYear() {
let today = new Date();
const newYear = new Date(today.getFullYear(),11,31);

if (today.getMonth() === 11 && today.getDate()>31) {
newYear.setFullYear(newYear.getFullYear()); 
}  
let one_day=1000*60*60*24;
let res = Math.ceil((newYear.getTime() - today.getTime())/one_day);
return res;
}
console.log(getAmountDaysToNewYear());

//3

const birthday17 = new Date(2004, 12, 29);
const birthday15 = new Date(2006, 12, 29);
const birthday22 = new Date(2000, 9, 22);

function getApproveToPass(bth){
    let today = Date.now();  
   let diff =(today - bth.getTime()) / 1000;
   diff /= 60 * 60 * 24;
   let yearsOld = Math.abs(Math.round(diff/365.25));
  let diffYear = 18-yearsOld;
  
    if(yearsOld >= 18){
        return 'Hello adventurer, you may pass!';
    } else if (diffYear > 1){
         return `Hello adventurer, you are to yang for this quest wait for ${diffYear} years more!`;
    } else {
 return 'Hello adventurer, you are to yang for this quest wait for few more months!';
    }
   
}
console.log(getApproveToPass(birthday17)); // Hello adventurer, you are to yang for this quest wait for few more months!
console.log(getApproveToPass(birthday15)); // Hello adventurer, you are to yang for this quest wait for 3 years more!
console.log(getApproveToPass(birthday22)); // Hello adventurer, you may pass!

//4
const elementP = 'tag="p" class="text" style={color: #aeaeae;} value="Aloha!"';

function transformStringToHtml(string) {
const [tagName] = string.match(new RegExp('(?<=tag=")[^"]+(?=")','g'));
const [tagValue] = string.match(new RegExp('(?<=value=")[^"]+(?=")', 'g'));
    const tagAttributes = string.replace(/(tag=".*?")/g, '').replace(/({|})/g, '"').replace(/(value=".*?")/g,'');
    return `<${tagName}${tagAttributes}>${tagValue}</${tagName}>`;
}
console.log(transformStringToHtml(elementP));


//5
function isValidIdentifier(variable) {
  
return /^[A-z_$][A-z_\d$]*$/g.test(variable);
   
    
}
console.log(isValidIdentifier('myVar!')); // false
console.log(isValidIdentifier('myVar$')); // true
console.log(isValidIdentifier('myVar_1')); // true
console.log(isValidIdentifier('1_myVar')); // false


//6
const testStr = 'My name is John Smith. I am 27.';
function capitalize(){
    const arr = testStr.split(' ');
    for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);

}

const str2 = arr.join(' ');
return str2;

}
console.log(capitalize(testStr));

//7
function isValidPassword(pw){
let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    return re.test(pw);
}
console.log(isValidPassword('agent007')); // false (no uppercase letter)
console.log(isValidPassword('AGENT007')); // false (no lowercase letter)
console.log(isValidPassword('AgentOOO')); // false (no numbers)
console.log(isValidPassword('Age_007')); // false (too short)
console.log(isValidPassword('Agent007')); // true


//8
function bubbleSort(inputArr){
    let len = inputArr.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (inputArr[j] > inputArr[j + 1]) {
                let tmp = inputArr[j];
                inputArr[j] = inputArr[j + 1];
                inputArr[j + 1] = tmp;
            }
        }
    }
    return inputArr;
}
console.log(bubbleSort([7,5,2,4,3,9]));



//9
const inventory = [
{ name: 'milk', brand: 'happyCow', price: 2.1 }, 
{ name: 'chocolate', brand: 'milka', price: 3 }, 
{ name: 'beer', brand: 'hineken', price: 2.2 }, 
{ name: 'soda', brand: 'coca-cola', price: 1 }
];

function sortByItem() {
return inventory.sort((a, b) => a.name > b.name ? 1 : -1);

}


console.log(sortByItem({item: 'name', array: inventory}));