import { dictionary } from './dictionary.js';
let height = 6; //number of guesses
let width = 5; //length of the word

let row = 0;
let col = 0;
let gameOver = false;

let rightGuessString = dictionary[Math.floor(Math.random()*dictionary.length)].toLowerCase();

document.getElementById('reset').onclick = () => {
    reset();
}

document.getElementById('check').onclick = () => {
    update();
}


console.log(rightGuessString);



window.onload = function(){
    intialize();
}

 //creating field for game
function intialize() {
    for (let r = 0; r < height; r++) {
        for (let c = 0; c < width; c++) {
            let tile = document.createElement('div');
            tile.id = r.toString() + '-' + c.toString();
            tile.classList.add('tile');
            tile.innerText = '';
            document.getElementById('input-field').appendChild(tile);
        }
    }
    
 //keyboard input   
document.addEventListener('keyup', (e) => { 
    if ('KeyA' <= e.key) {
        if (col < width) {
            let currTile = document.getElementById(row.toString() + '-' + col.toString());
            if (currTile.innerText === '') {
                currTile.innerText = e.key; 
                col += 1;
            }
        }
    } else if (e.code === 'Backspace') {
        if (0 < col && col <= width) {
            col -=1;
        }
        let currTile = document.getElementById(row.toString() + '-' + col.toString());
        currTile.innerText = '';
    } else if (e.code=== 'Enter') {
        
        update();
    }

    if (!gameOver && row === height) {
        gameOver = true;
     
    }
    
});
}

   
   // main function
function update() {
    //check word 
       let guess = '';
       for (let c = 0; c < width; c++) {
        let currTile = document.getElementById(row.toString() + '-' + c.toString());
        let letter = currTile.innerText;
        guess += letter;
          
     }
     guess = guess.toLowerCase();

    if (guess.length !== 5) {
        alert('Not enough letters!')
        return
    }

    if (!dictionary.includes(guess)) {
        alert('Word not in list!')
        return
    }

     
//check letter and change background into right color
    let correct = 0;
    let rightWord = Array.from(rightGuessString);

    for (let i = 0; i < rightGuessString.length; i++) {
        
        for (let c = 0; c < width; c++) {
        let currTile = document.getElementById(row.toString() + '-' + c.toString());
        let alpha = currTile.innerText;

      
        if (rightGuessString[c] === alpha) {
            currTile.classList.add('correct');
            correct += 1;
            rightWord[alpha] -= 1; 
        }

        if (correct === width) {
            gameOver = true;
        }
    }
        for (let c = 0; c < width; c++) {
        let currTile = document.getElementById(row.toString() + '-' + c.toString());
        let alpha = currTile.innerText;

      
        if (!currTile.classList.contains('correct')) {
            
            if (!rightGuessString.includes(alpha)) {
                currTile.classList.add('absent');
                
            } else {
                currTile.classList.add('present');
                
            }
        }
    }
       
         
if (guess === rightGuessString) {
        alert('You guessed right! Game over!')
        return
   } 

}
    if (row < 5){ //next line, if it was last line show messages 
        row += 1;
        col = 0;
    } else {
        alert('You are out of guess')
    }
}

//reload page
function reset(){
 return document.location.reload();
}