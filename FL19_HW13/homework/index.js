window.addEventListener('DOMContentLoaded', () => {

    const playerDisplay = document.querySelector('.display-player');
    const resetButton = document.querySelector('#reset');
    const announcer = document.querySelector('.announcer');



    let board = ['', '', '', '', '', '', '', '', ''];
    let currentPlayer = 'X';
    let isGameActive = true;
    let size = 9;
    const PLAYERX_WON = 'PLAYERX_WON';
    const PLAYERO_WON = 'PLAYERO_WON';
    const TIE = 'TIE';



    const winningConditions = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];


    const dragAndDrop = function () {

    const imges = document.querySelectorAll('.avatar-icon'); //images container
    const img_location =document.querySelectorAll('.avatar-container'); //location
    const backHome = document.querySelector('.icons');


       let dragItem = null;
        function dragStart() {
            console.log('drag started');
            dragItem = this;
            setTimeout(() => {
            this.className = 'hide', 0
            });

        }
        function dragEnd() {
            console.log('drag ended');
            this.classList.remove('hide');
            this.className = 'avatar-icon'

        }
        function dragOver(e) {

             e.preventDefault();
            console.log('drag over');

        }

        function dragEnter() {
            this.classList.remove('hide');
            console.log('drag entered');
        }

        function dragLeave(e) {
            e.preventDefault();
            console.log('drag left');

        }

        function dragDrop(e) {
             this.append(dragItem);
             console.log(e.target);

        }
        imges.forEach(img => {
            img.addEventListener('dragstart', dragStart)
            img.addEventListener('dragend', dragEnd)
        });

        img_location.forEach(loc => {
            loc.addEventListener('dragover', dragOver);
            loc.addEventListener('dragenter', dragEnter);
            loc.addEventListener('dragleave', dragLeave);
            loc.addEventListener('drop', dragDrop);
        });
        backHome.addEventListener('dragover', dragOver);
            backHome.addEventListener('dragenter', dragEnter);
            backHome.addEventListener('dragleave', dragLeave);
            backHome.addEventListener('drop', dragDrop);
    };


    dragAndDrop();

    gameBoard();

    function gameBoard() {
        for (let r = 0; r < size; r++) {

            let container = document.getElementsByClassName('container');
            let tile = document.createElement('div');
            tile.classList.add('tile');
            tile.innerText = '';
            container[0].appendChild(tile);


        }

    }


    const tiles = Array.from(document.querySelectorAll('.tile'));

    function handleResultValidation() {
        let roundWon = false;
        for (let i = 0; i <= 7; i++) {
            const winCondition = winningConditions[i];
            const a = board[winCondition[0]];
            const b = board[winCondition[1]];
            const c = board[winCondition[2]];
            if (a === '' || b === '' || c === '') {
                continue;
            }
            if (a === b && b === c) {
                roundWon = true;
                break;
            }
        }

        if (roundWon) {
            announce(currentPlayer === 'X' ? PLAYERX_WON : PLAYERO_WON);
            isGameActive = false;
            return;
        }

        if (!board.includes('')){
            announce(TIE);
        }
    }

    const announce = (type) => {
        switch (type) {
            case PLAYERO_WON:
                announcer.innerHTML = 'Player <span class="playerO">O</span> Won';
                break;
            case PLAYERX_WON:
                announcer.innerHTML = 'Player <span class="playerX">X</span> Won';
                break;
            case TIE:
                announcer.innerText = 'Tie';
        }
        announcer.classList.remove('hide');
    };

    const isValidAction = (tile) => {
        if (tile.innerText === 'X' || tile.innerText === 'O') {

            return false;
        }

        return true;
    };

    const updateBoard = (index) => {
        board[index] = currentPlayer;

    }



    const changePlayer = () => {
        playerDisplay.classList.remove(`player${currentPlayer}`);
        currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
        playerDisplay.innerText = currentPlayer;
        playerDisplay.classList.add(`player${currentPlayer}`);
    }


    const userAction = (tile, index) => {
        if (isValidAction(tile) && isGameActive) {
            tile.innerText = currentPlayer;
            tile.classList.add(`player${currentPlayer}`);
            updateBoard(index);
            handleResultValidation();
            changePlayer();

            setTimeout(() => {
                tile.classList.remove('active');
            },500)

        }
    }

    const resetBoard = () => {
        board = ['', '', '', '', '', '', '', '', ''];
        isGameActive = true;
        announcer.classList.add('hide');

        if (currentPlayer === 'O') {
            changePlayer();
        }

        tiles.forEach(tile => {
            tile.innerText = '';
            tile.classList.remove('playerX');
            tile.classList.remove('playerO');
        });
    }

    tiles.forEach((tile, index) => {
        tile.addEventListener('click', () => userAction(tile, index));

    });



    resetButton.addEventListener('click', resetBoard);
});
