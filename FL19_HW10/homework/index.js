let clicks = 0;
let result = 0;
const TIMEOUT = 5000;  
const click_btn = document.getElementById('click-me-btn');
const nickname = document.getElementById('user-name'); 
const best_result = document.getElementById('best-result').onclick = () => {
 
    const res = localStorage.getItem(userName) || 0;
    alert(`Best result is ${res}`);
        
};
const best_resultAll = document.getElementById('best-result-for-all-time').onclick = () => {
    const res = localStorage.getItem(userName) || 0;
    alert(`Best result is ${res} by ${userName}`);
}
const clear_result = document.getElementById('clear-best-result').onclick = () => {
  localStorage.removeItem(userName, clicks);
   alert('Best result deleted');
};
const clear_resultALL = document.getElementById('clear-best-result-for-alltime').onclick = () => {
    localStorage.clear(userName, clicks);
       alert('Best result deleted');
};
const start = document.getElementById('start').onclick = () => {
    startbtn()
};
let userName = '';



function startbtn (){
 
   
    try{
  if (!nickname.value.trim()){
   throw new Error();
  }  
   startGame();
    
} catch(err) {
    alert('Empty nickname');
}
 }   


function startGame(){
userName = nickname.value;
 
 click_btn.onclick = () => {
   clicks++;
 }
 
 const settime = setTimeout(() => {
    alert(`You clicked ${clicks} times`);
    localStorage.setItem(userName, JSON.stringify(clicks));
    const getValue = localStorage.getItem(userName);
    result = JSON.parse(getValue);
     clicks = 0;
    click_btn.onclick = null;
     clearTimeout(settime);
    }, TIMEOUT);
    
    
  }

