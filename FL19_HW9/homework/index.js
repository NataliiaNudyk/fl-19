// #1
function calculateSum(arr) {
let sum = 0;
 for (let i = 0; i < arr.length; i++){
   sum += arr[i];
 } 
    return sum;
   
}

console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {

return a < b + c && a + b > c && a + c > b;
}

console.log(isTriangle(5,6,7)); //true
console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
    return word.split('').filter((item, pos, word) => word.indexOf(item) === pos).length === word.length;
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('abab')); //false

// #4
function isPalindrome(word) {
const word2 = word.toLowerCase().split('');
const wordreverse = word.toLowerCase().split('').reverse();
let res = false;
    for (let i = 0; i < word2.length; i++){
    for (let j = 0; j < wordreverse.length; j++){
         if(word2[i] === wordreverse[j]){
        res = true;
    } else {
        res = false;
    }   
        }
    }
    
   return res;
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
    const date = new Date(dateObj);
    const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const monthName = months[month]
    return `${day} of ${monthName}, ${year}`;

}



console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => {
let res = 0;
 for (let i = 0; i < str.length; i++) {
    if (str.charAt(i) === letter) {
      res += 1;
      }
  }
  return res;
}


console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
const counts = {};
arr.forEach(function (x) { 
counts[x] = (counts[x] || 0) + 1; 
});
    return counts;
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
   const binaryString = arr.join('');
   return parseInt(binaryString, 2);

}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9

